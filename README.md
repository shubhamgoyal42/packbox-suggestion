
## Problem:

    A set of items and a set of boxes of cuboidal shape are given.
    Find the box of least volume that can accommodate all items.

## Steps:

 - Increase dimensions of all items by 10% and volume by 20%.(Packing allowance)
 - For all items and boxes, let greatest dimension be length, next greatest dimension be width, and last dimension be height.(Since all cuboids can be re-oriented)
 - Eliminate certain boxes:
    - Remove boxes for which total item volume > box volume.
    - Remove boxes for which greatest item length > box length.
    - Remove boxes for which greatest item width > box width.
    - Remove boxes for which greatest item height > box height.
 - Sort boxes in increasing order of their volume.
 - Sort items first on basis of length, then width, then height.
 - Iterate among boxes:
    - Initiate coordinates at X=0; Y=0; Z=0. These represent distance from origin till which point box space is consumed.
    - Initiate coordinates of last added item at xLast=0; yLast=0; zLast=0.
    - Iterate among items:
        ``` 
        If X + lengthOf(item) < lengthOf(box):
            X = X + lengthOf(item)
            If widthOf(item) > yLast:
                Y = Y + (widthOf(item) - yLast)
            If heightOf(item) > zLast:
                Z = Z + (heightOf(item) - zLast)

        Else If Y + widthOf(item) < widthOf(box):
            Y = Y + widthOf(item)
            If heightOf(item) > zLast:
                Z = Z + (heightOf(item) - zLast)

        Else If Z + heightOf(item) < heightOf(box):
            Z = Z + heightOf(item)

        Else
            Fail(start with next box)
        ```
